# Stealth Commander

## Sumário
* [Introdução](#introdução)
* [Pré-requisitos](#pré-requisitos)
* [Clonar](#clonar)
* [Artigo](#artigo)
* [Download da Demo](#download-da-demo)
* [Documentação](#documentação)
* [Licença](#licença)

# Introdução
O "Stealth Commander" será um software desenvolvido utilizando Unity(motor de jogo 
criado pela Unity Technologies):
Embora ainda seja esteja em fase de desenvolvimeto, ele oferece uma visão ampla
de um game com conceito simples usando o motor escolhido e em breve o mesmo 
poderá ser colocado em produção atendendo as espectativas de um possível cliente
pois, o mesmo não será somente um protótipo fictício como requisito da disciplina
de Projeto de Desenvolvimento, e sim um game completo para ser lançado
oficialmente. Sendo assim, o projeto (Stealth Commander) possibilitara ao jogador
uma experiência simples e nostalgica, tendo como inspirações jogos clássicos, 
retornando a era 16 bits.

O projeto começou ser desenvolvido na aula e 
faz parte da avaliação da disciplina de Projeto de Desenvolvimento do
5° semestre da Faculdade de Tecnologia Senac Pelotas (RS),.
O projeto conta também com a orientação do prof. Angelo Luz e o andamento do
mesmo em breve poderá ser acompanhado pelo gitlab ou outro!

🚧 **Situação atual do projeto: em construção/desenvolvimento!**

**Sinta-se livre para clonar ou baixar o código-fonte do projeto.**

## Pré-requisitos
O Unity assim como outras ferramentas necessitam de alguns requisitos de sistema.
O que varia muito dependendo da complexidade do projeto.
Para mais informações acesse o site.
**Importante:** O Unity está disponível para Mac OS X e Windows, não sendo possível
assim ele ser utilizado no Linux.
O Unity possui versões pagas, mas a versão **gratuita** é mais do que suficiente para
o desenvolvimento do projeto.
Caso queiram testar o projeto na Unity, o game está sendo desenvolvido na
versão Unity 2018.3.9, sugiro que utilize a partir desta versão ou posteriores
(versões mais antigas podem não abrir o projeto).
Atualmente o Unity esta na versão 2018.3.10.

⚠ **ATENÇÃO, as ferramentas, comandos, linguagens  e etc. requerem conhecimentos prévios!**

[Unity Requisitos do Sistema](https://unity3d.com/pt/unity/system-requirements)

[Site do Unity](https://unity.com/pt)

[Download do Unity](https://store.unity.com/?_ga=2.126865934.249640677.1553827386-1206357891.1546796747)


## Clonar

Para efetuar o clone do projeto usando os *links* disponíveis no repositório 
GitLab é preciso possuir o *software* Git instalado na sua máquina. 
Para saber como instalar e usar a ferramenta Git acesse o *site* do 
Git: <https://git-scm.com/doc>. Caso já saiba como fazer use os comandos abaixo:

**Com SSH:**

```bash
$ git clone git@gitlab.com:senac-tcc-2019/grazziano.git
```

ou 

**Com HTTPS:**

```bash
$ git clone https://gitlab.com/senac-tcc-2019/grazziano.git
```

ou você pode baixar o projeto (*download*): [zip](https://gitlab.com/senac-tcc-2019/grazziano/-/archive/master/grazziano-master.zip),
[tar.gz](https://gitlab.com/senac-tcc-2019/grazziano/-/archive/master/grazziano-master.tar.gz),
[tar.bz2](https://gitlab.com/senac-tcc-2019/grazziano/-/archive/master/grazziano-master.tar.bz2) e 
[tar](https://gitlab.com/senac-tcc-2019/grazziano/-/archive/master/grazziano-master.tar).

## Download da Demo

[Baixe aqui o executável do jogo](https://drive.google.com/drive/folders/1guatybTJyHBL6-z8-lAV4lbZD0UaFfor?usp=sharing)

## Artigo

[Artigo](https://www.overleaf.com/read/zjjmdwqdmmgq)

## Documentação

A documentação com maior clareza se encontra na **[wiki](https://gitlab.com/senac-tcc-2019/grazziano/wikis/home)** desse repositório.

## Licença

A aplicação conta com a seguinte licença de uso: **[MIT License](https://choosealicense.com/licenses/mit/).**
