﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CristalCollect : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            //Score.scoreValue += 10; // incrementa o score
            Score.scoreValue += Random.Range(5, 10); // incrementa o score com número randomico entre 10 e 20
            Destroy(gameObject); // destrói o objeto
        }
    }
}
