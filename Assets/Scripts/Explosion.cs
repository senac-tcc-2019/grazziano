﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{

    public int damage = 5;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy otherEnemy = collision.GetComponent<Enemy>();
        Boss boss = collision.GetComponent<Boss>();

        if (otherEnemy != null)
        {
            otherEnemy.TookDamage(damage); // passa o dano da explosão
        }

        if (boss != null)
        {
            boss.TookDamage(damage); // passa o dano do tiro
        }
    }
}
