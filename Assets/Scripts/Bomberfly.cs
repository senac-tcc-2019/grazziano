﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomberfly : Enemy
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update(); // Chama o script pai

        if (Mathf.Abs(targetDistance) < attackDistance)
        {
            // Segue o player
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
        }
    }
}
