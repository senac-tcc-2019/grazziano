﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverControl : MonoBehaviour
{
   /* public static int scoreValue = 0;
    public Text score;
    public Text highScore;*/

    public TextMesh score;
    public TextMesh highScore;

    // Start is called before the first frame update
    void Start()
    {
        //score = GetComponent<Text>();
        //highScore = GetComponent<Text>();
        score.text = Score.scoreValue.ToString();
        highScore.text = PlayerPrefs.GetInt("Highscore").ToString();
    }

    // Update is called once per frame
    void Update()
    {
        //score.text = "Score: " + Score.scoreValue;
        //highScore.text = "Recorde: " + PlayerPrefs.GetInt("Highscore").ToString();

        if (Input.GetButtonDown("Jump"))
        {
            Application.LoadLevel("Menu");
        }
    }
}
