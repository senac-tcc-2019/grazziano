﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITweenFade : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("EfeitoAlpha");
        //Destroy(gameObject, 5);
    }

    IEnumerator EfeitoAlpha()
    {
        yield return new WaitForSeconds(2);
        iTween.FadeTo(gameObject, iTween.Hash("alpha", 0, "delay", 0.01f, "time", 0.02f, "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.easeInElastic));
    }
}
