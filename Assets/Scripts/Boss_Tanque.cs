﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss_Tanque : MonoBehaviour
{
    public Rigidbody2D bullet;
    public Transform[] shotSpawners;
    private bool isDead = false;
    public float minYForce, maxYForce;
    public float fireRateMin, fireRateMax;
    public GameObject enemy;
    public Transform enemySpawn;
    public float minEnemyTime, maxEnemyTime;
    public int health;
    private SpriteRenderer sprite;

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        ActivateBoss();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Fire()
    {
        if (!isDead)
        {
            Rigidbody2D tempBullet = Instantiate(bullet, shotSpawners[Random.Range(0, shotSpawners.Length)].position, Quaternion.identity);
            tempBullet.AddForce(new Vector2(0, Random.Range(minYForce, maxYForce)), ForceMode2D.Impulse);
            Invoke("Fire", Random.Range(fireRateMin, fireRateMax));
        }
    }

    void InstantiateEnemies()
    {
        if (!isDead)
        {
            Instantiate(enemy, enemySpawn.position, enemySpawn.rotation);
            Invoke("InstantiateEnemies", Random.Range(minEnemyTime, maxEnemyTime));
        }
    }

    public void ActivateBoss()
    {
        GetComponent<BoxCollider2D>().enabled = true;
        Invoke("Fire", Random.Range(fireRateMin, fireRateMax));
        Invoke("InstantiateEnemies", Random.Range(minEnemyTime, maxEnemyTime));
    }

    public void TookDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            isDead = true;
            Enemy[] enemies = FindObjectsOfType<Enemy>();

            foreach(Enemy enemy in enemies)
            {
                enemy.gameObject.SetActive(false);
            }

            Bullet[] bullets = FindObjectsOfType<Bullet>();
            foreach (Bullet bullet in bullets)
            {
                bullet.gameObject.SetActive(false);
            }

            Score.scoreValue += Random.Range(1, 10);  // Incrementa o score
            // Instantiate(coin, transform.position, transform.rotation);
            // Instantiate(deathAnimation, transform.position, transform.rotation);

            // gameObject.SetActive(false);
            Invoke("LoadScene", 2f);
        }
        else
        {
            StartCoroutine(TookDamageCoRoutine());
        }
    }

    IEnumerator TookDamageCoRoutine()
    {
        sprite.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        sprite.color = Color.white;
    }

    void LoadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
    }
}
