﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldado : MonoBehaviour
{

    private float velMove = 2f;
    private Rigidbody2D rb;
    private bool moveE;
    [SerializeField]
    private Transform limite;
    public LayerMask layer;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        Physics2D.IgnoreLayerCollision(10, 10);
        rb = GetComponent<Rigidbody2D>();
        moveE = true;
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (moveE)
        {
            rb.velocity = new Vector2(-velMove, rb.velocity.y);
        }
        else
        {
            rb.velocity = new Vector2(velMove, rb.velocity.y);
        }

        VerificaCol();
    }

    void VerificaCol()
    {
        if (!Physics2D.Raycast(limite.position, Vector2.down, 0.1f, layer))
        {
            Flip();
        }
    }

    void Flip()
    {
        moveE = !moveE;
        Vector3 temp = transform.localScale;

        if (moveE)
        {
            temp.x = Mathf.Abs(temp.x);
        }
        else
        {
            temp.x = -Mathf.Abs(temp.x);
        }

        transform.localScale = temp;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            anim.Play("Die");
            Destroy(gameObject, 0.6f);
            Destroy(collision.gameObject);
        }
    }
}
