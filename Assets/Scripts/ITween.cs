﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITween : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //iTween.MoveTo(gameObject, new Vector3(0, 0, 0), 2);
        iTween.MoveTo(gameObject, iTween.Hash("x", 0, "y", 10, "time", 2, "looptype", iTween.LoopType.pingPong, "easytype", iTween.EaseType.easeInOutElastic));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
