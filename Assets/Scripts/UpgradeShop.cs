﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeShop : MonoBehaviour
{
    public Text healthText, damageText, fireRateText, bulletsText, reloadingTimeText, upgradeCostText;
    GameManager gameManager;
    Player player;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.gameManager;
        player = FindObjectOfType<Player>();
    }

    void UpdateUI()
    {
        healthText.text = "Vida: " + gameManager.health;
        damageText.text = "Dano: " + gameManager.damage;
        fireRateText.text = "Cadência de Tiro: " + gameManager.fireRate;
        bulletsText.text = "Total de Tiros: " + gameManager.bullets;
        reloadingTimeText.text = "Tempo de Recarga: " + gameManager.reloadTime;
        upgradeCostText.text = "Preço do Upgrade: " + gameManager.upgradeCost;
    }

    public void SetHealth()
    {
        if (gameManager.coins >= gameManager.upgradeCost)
        {
            gameManager.health++;
            FindObjectOfType<UIManager>().UpdateHealthBar();
            player.SetPlayerStatus();
            SetCoins(gameManager.upgradeCost);
            gameManager.upgradeCost += (gameManager.upgradeCost / 5); // Aumenta custo do upgrade em 20%
            UpdateUI();
        }
    }

    public void SetDamage()
    {
        if (gameManager.coins >= gameManager.upgradeCost)
        {
            gameManager.damage++;

            player.SetPlayerStatus();
            SetCoins(gameManager.upgradeCost);
            gameManager.upgradeCost += (gameManager.upgradeCost / 5); // Aumenta custo do upgrade em 20%
            UpdateUI();
        }
    }

    public void SetFireRate()
    {
        if (gameManager.coins >= gameManager.upgradeCost)
        {
            gameManager.fireRate -= 0.1f;
            if (gameManager.fireRate <= 0)
            {
                gameManager.fireRate = 0;
            }

            player.SetPlayerStatus();
            SetCoins(gameManager.upgradeCost);
            gameManager.upgradeCost += (gameManager.upgradeCost / 5); // Aumenta custo do upgrade em 20%
            UpdateUI();
        }
    }

    public void SetBullets()
    {
        if (gameManager.coins >= gameManager.upgradeCost)
        {
            gameManager.bullets++;

            player.SetPlayerStatus();
            SetCoins(gameManager.upgradeCost);
            gameManager.upgradeCost += (gameManager.upgradeCost / 5); // Aumenta custo do upgrade em 20%
            UpdateUI();
        }
    }

    public void SetReloadingTime()
    {
        if (gameManager.coins >= gameManager.upgradeCost)
        {
            gameManager.reloadTime -= 0.1f;
            if (gameManager.reloadTime <= 0)
            {
                gameManager.reloadTime = 0;
            }

            player.SetPlayerStatus();
            SetCoins(gameManager.upgradeCost);
            gameManager.upgradeCost += (gameManager.upgradeCost / 5); // Aumenta custo do upgrade em 20%
            UpdateUI();
        }
    }

    void SetCoins(int coin)
    {
        gameManager.coins -= coin;
        FindObjectOfType<UIManager>().UpdateCoins();
    }
}
