﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    public static int scoreValue = 0;
    Text score;
    public Text highscore;

    // Start is called before the first frame update
    void Start()
    {
        score = GetComponent<Text>();
        highscore.text = "High Score: " + PlayerPrefs.GetInt("Highscore", 0).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        score.text = "Score: " + scoreValue;

        if (scoreValue > PlayerPrefs.GetInt("Highscore", 0))
        {
            PlayerPrefs.SetInt("Highscore", scoreValue);
            highscore.text = "High Score: " + scoreValue;
        }
    }

    public void Reset()
    {
        //PlayerPrefs.DeleteKey("Highscore");
        PlayerPrefs.DeleteAll();
    }
}
