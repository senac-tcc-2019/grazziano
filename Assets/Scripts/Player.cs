﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // Biblioteca responsavel por reccaregar a cena quanndo personagem morrer
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float speed = 6f;
    public float jumpForce = 800;
    public GameObject bulletPrefab;
    public Transform shotSpawner;
    public Rigidbody2D bomb;
    public float damageTime = 1f;    // Tempo de invulnerabilidade após tomar dano
    public bool canFire = true;  // Variável para não deixar player atirar enquando está na loja de upgrade
    private Animator anim;
    private Rigidbody2D rb2d;
    private bool facingRight = true;
    private bool jump;
    private bool onGround = false;
    private Transform groundCheck;
    private float hForce = 0;
    private bool isDead;
    private bool crouched;
    private bool lookingUp;
    private bool reloading;
    private float fireRate = 0.5f;
    private float nextFire;    // Controla quando pode atirar novamente    
    private bool tookDamage = false; // Controla se player sofreu dano ou não
    /*Variáveis do GameManager*/
    GameManager gameManager;
    private int bullets;
    private float reloadingTime;
    private int health;
    private int maxHealth;
    private int bombs;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        groundCheck = gameObject.transform.Find("GroundCheck");
        anim = GetComponent<Animator>();
        gameManager = GameManager.gameManager;

        SetPlayerStatus();

        bombs = gameManager.bombs;
        health = maxHealth;

        UpdateBulletsUI();
        UpdateBombsUI();
        UpdateHealthUI();
    }

    // Update is called once per frame
    void Update()
    {

 
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }               

    
        if (!isDead)
        {
            onGround = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

            if (onGround)
            {
                anim.SetBool("Jump", false);
            }

            if (Input.GetButtonDown("Jump") && onGround && !reloading)
            {
                jump = true;
            }else if (Input.GetButtonUp("Jump"))
            {
                if (rb2d.velocity.y > 0)
                {
                    rb2d.velocity = new Vector2(rb2d.velocity.x, rb2d.velocity.y * 0.5f);
                }
            }

            if (Input.GetButtonDown("Fire1") && Time.time > nextFire && bullets > 0 && !reloading && canFire)
            {
                nextFire = Time.time + fireRate;
                anim.SetTrigger("Shoot");
                GameObject tempBullet = Instantiate(bulletPrefab, shotSpawner.position, shotSpawner.rotation);

                if (!facingRight && !lookingUp)
                {
                    tempBullet.transform.eulerAngles = new Vector3(0, 0, 180); // Muda a trajetória do tiro para esquerda quando sprite não está virado para a direita
                }else if (!facingRight && lookingUp)
                {
                    tempBullet.transform.eulerAngles = new Vector3(0, 0, 90); // Muda a trajetória do tiro para cima quando sprite não está virado para a direita e olha para cima
                }

                if (crouched && !onGround)  // se esta apontando para baixo e não está no chão
                {
                    tempBullet.transform.eulerAngles = new Vector3(0, 0, -90);  // muda a trajetória do tiro para baixo
                }

                bullets--;

                UpdateBulletsUI();

            }else if(Input.GetButtonDown("Fire1")&& bullets <= 0 && onGround)
            {
                StartCoroutine(Reloading());
            }

            lookingUp = Input.GetButton("Up");
            crouched = Input.GetButton("Down");

            anim.SetBool("Looking Up", lookingUp);
            anim.SetBool("Crouched", crouched);

            if (Input.GetButtonDown("Reloading") && onGround)
            {
                /*reloading = true;
                anim.SetBool("Reloading", true);*/
                StartCoroutine(Reloading());
            }

            if (Input.GetButtonDown("Fire2") && bombs > 0)
            {
                Rigidbody2D tempBomb = Instantiate(bomb, transform.position, transform.rotation);

                if (facingRight)
                {
                    tempBomb.AddForce(new Vector2(8, 10), ForceMode2D.Impulse);
                }
                else
                {
                    tempBomb.AddForce(new Vector2(-8, 10), ForceMode2D.Impulse);
                }
                bombs--;

                UpdateBombsUI();
            }

            if ((crouched || lookingUp || reloading) && onGround)
            {
                hForce = 0;
            }
        }
    }

    private void FixedUpdate()
    {
        if (!isDead)
        {
            if (!crouched && !lookingUp && !reloading)
            hForce = Input.GetAxisRaw("Horizontal");

            anim.SetFloat("Speed", Mathf.Abs(hForce)); // Quando hForce for < ou > do que 0 tranforma pra positivo e passa o valor para a variável speed dentro do animator
            rb2d.velocity = new Vector2(hForce * speed, rb2d.velocity.y);

            if (hForce > 0 && !facingRight)
            {
                Flip();
            }else if (hForce < 0 && facingRight)
            {
                Flip();
            }

            if (jump)
            {
                anim.SetBool("Jump", true);
                jump = false;
                rb2d.AddForce(Vector2.up * jumpForce);
            }
        }
    }

    void Flip()
    {
        facingRight = !facingRight;

        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    public void SetPlayerStatus()
    {
        fireRate = gameManager.fireRate;
        bullets = gameManager.bullets;
        reloadingTime = gameManager.reloadTime;
        maxHealth = gameManager.health;
    }

    void UpdateBulletsUI()
    {
        FindObjectOfType<UIManager>().UpdateBulletsUI(bullets);
    }

    IEnumerator Reloading()
    {
        reloading = true;
        anim.SetBool("Reloading", true);
        yield return new WaitForSeconds(reloadingTime);
        bullets = gameManager.bullets;
        reloading = false;
        anim.SetBool("Reloading", false);
        UpdateBulletsUI();
    }

    void UpdateBombsUI()
    {
        FindObjectOfType<UIManager>().UpdateBombs(bombs);
        gameManager.bombs = bombs;
    }

    void UpdateHealthUI()
    {
        FindObjectOfType<UIManager>().UpdateHealthUI(health);
    }

    void UpdateCoinsUI() // Método para atualizar moedas na tela
    {
        FindObjectOfType<UIManager>().UpdateCoins();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy") && !tookDamage)
        {
            StartCoroutine(TookDamage());
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy") && !tookDamage)  // se houver colisão com objetos com a tag "Enemy" 
        {
            StartCoroutine(TookDamage());
        }
        else if (collision.gameObject.CompareTag("Coin"))  // se houver colisão com objetos com a tag "Coin" 
        {
            Destroy(collision.gameObject);  // destroi moeda
            gameManager.coins += 1;  // incrementa moedas
            Score.scoreValue += Random.Range(1, 5); // incrementa o score
            UpdateCoinsUI();   // atualiza moedas na tela
        }
    }

    IEnumerator TookDamage()
    {
        tookDamage = true;
        health--;
        UpdateHealthUI();
        if (health <= 0)
        {
            isDead = true;
            anim.SetTrigger("Death");
            // Invoke("ReloadScene", 2f);
            yield return new WaitForSeconds(reloadingTime);


            Application.LoadLevel("GameOver"); // Carrega cena GameOver quando player morre
        }
        else
        {
            Physics2D.IgnoreLayerCollision(9, 10); // se tomou dano for true ignora colisão
            for (float i = 0; i < damageTime; i += 0.2f)
            {
                GetComponent<SpriteRenderer>().enabled = false;
                yield return new WaitForSeconds(0.1f);
                GetComponent<SpriteRenderer>().enabled = true;
                yield return new WaitForSeconds(0.1f);
            }
            Physics2D.IgnoreLayerCollision(9, 10, false);
            tookDamage = false;
        }
    }

    void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
    }

    public void SetHealthAndBombs(int life, int bomb) // Função para atualizar vida e bombas
    {
        health += life;

        if (health >= maxHealth)
        {
            health = maxHealth;
        }

        bombs += bomb;  // Atualiza valor de bombas

        UpdateBombsUI();  // atualiza valor de bombas na tela
        UpdateHealthUI(); // atualiza valor de vida na tela
    }

}
