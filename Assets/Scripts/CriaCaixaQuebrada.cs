﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CriaCaixaQuebrada : MonoBehaviour
{
    [SerializeField]
    private GameObject cxQuebrada;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            Instantiate(cxQuebrada, transform.position, Quaternion.identity);
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }
}
